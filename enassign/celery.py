from __future__ import absolute_import

import os
from celery import Celery
# from .settings import CELERY_BROKER_URL
# from .settings import BROKER_URL

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'enassign.settings')

app = Celery('enassign',
            backend='amqp://',
            include=['enassign'])
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()