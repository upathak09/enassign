# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class AuctionUserConfig(AppConfig):
    name = 'auction_user'
