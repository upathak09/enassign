# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

from django.core.validators import validate_email

from enassign.custom_fields import (CaseInsensitiveCharField,
        CaseInsensitiveEmailField)

from enassign.custom_models import BaseModel

# Create your models here.

class User(AbstractBaseUser, BaseModel):
    """
        An abstract base class implementing a fully featured User model with
        admin-compliant permissions.

        email is required. Other fields are optional.
    """

    email = CaseInsensitiveEmailField('Email', blank=False, null=False,
            unique=True, validators=[validate_email])
    name = models.CharField('Name', max_length=128, blank=False, null=False)
    objects = BaseUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return self.email