import json

from rest_framework import serializers

from .models import (AuctionItem, Bids)

from auction_user.models import User

from .tasks import find_winner


class AuctionSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    
    def get_status(self, instance):
        return instance.get_status_display()

    def get_image_url(self, instance):
        if instance.image:
            return instance.image.url
        return None

    def create(self, validated_data):
        if 'image' in self.context:
            image = self.context.pop('image')
        instance = AuctionItem.objects.create(**validated_data)
        instance.image = image
        instance.save()
        find_winner.apply_async(args=[instance.pk], eta=instance.end_time)
        return instance


    class Meta:
        model = AuctionItem
        read_only_fields = ('winner', 'id')
        write_only_fields = ('image', )
        fields = ('status', 'starting_amount', 'start_time', 'end_time',
            'id', 'winner', 'image_url')


class BidSerializer(serializers.ModelSerializer):
    user_id = serializers.PrimaryKeyRelatedField(
            many=True, read_only=True)
    item_id = serializers.PrimaryKeyRelatedField(
            many=True, read_only=True)

    def create(self, validated_data):
        validated_data['user_id'] = self.context.pop('user')
        validated_data['item_id'] = self.context.pop('item')
        bid = Bids.objects.create(**validated_data)
        return bid

    class Meta:
        model = Bids
        fields = ('user_id', 'item_id', 'bid_amount')










# class ActivitySerializer(serializers.ModelSerializer):
#     status = serializers.SerializerMethodField()
#     taken_by = serializers.PrimaryKeyRelatedField(
#             many=True, read_only=True)
#     activity_display = serializers.SerializerMethodField()

#     def get_status(self, instance):
#         return instance.get_status_display()

#     def get_activity_display(self, instance):
#         return instance.get_activity_mode_display()

#     def create(self, validated_data):
#         instance = Activity.objects.create(**validated_data)
#         return instance

#     def update(self, instance, validated_data):
#         taken_by = json.loads(self.context.pop('taken_by'))
#         post_id = self.context.pop('post')
#         post = Post.objects.get(pk=post_id)
#         post.activity_map.add(instance)
#         user = User.objects.filter(pk__in=taken_by)
#         for field in [k for k in validated_data]:
#             setattr(instance, field, validated_data[field])
#         instance.taken_by.add(*user)
#         instance.save()
#         activity_notification.delay(post_id, user[0].pk,
#             instance.activity_mode, user[0].name)
#         return instance

#     class Meta:
#         model = Activity
#         fields = ('status', 'taken_by', 'activity_content', 'activity_mode',
#                 'activity_display', 'id')


# class PostSerializer(serializers.ModelSerializer):

#     status = serializers.SerializerMethodField()
#     activity_map = serializers.PrimaryKeyRelatedField(
#                 many=True, read_only=True)

#     def get_status(self, instance):
#         return instance.get_status_display()

#     class Meta:
#         model = Post
#         fields = ('content', 'posted_by', 'activity_map', 'status', 'id')