from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from .views import (AuctionView, BidsView)

router = DefaultRouter()
router.register(r'item', AuctionView, base_name='item')
# router.register(r'useractivity', ActivityView, base_name='useractivity')

urlpatterns = [
	url(r'^bids/$', BidsView.as_view(), name='bids'),
    url(r'^', include(router.urls)),

]
