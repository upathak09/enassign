from .models import AuctionItem, Bids
from datetime import datetime
from dateutil import parser
from .constants import *
from email_sender import send_email_sendgrid

def modify_auction_response(serializer):
	datetime_now = datetime.now()
	return_dict = {'previous_auctions': [], 'upcoming_auctions': [],
		'ongoing_auctions': []}
	for item in serializer.data:
		start_time = end_time = None
		for field, value in item.iteritems():
			if field == 'end_time':
				end_time = parser.parse(value)
				if end_time < datetime_now:
					return_dict['previous_auctions'].append(item)
					break
			elif field == 'start_time':
				start_time = parser.parse(value)
				if start_time > datetime_now:
					return_dict['upcoming_auctions'].append(item)
					break
			if start_time and end_time:
				if start_time <= datetime_now <= end_time:
					return_dict['ongoing_auctions'].append(item)
					break
	return return_dict


def item_bid_mapping(serializer):
	datetime_now = datetime.now()
	serializer_data, return_dict = serializer.data, {}
	winner = serializer_data.get('winner', None)
	if winner is not None:
		try:
			bid = Bids.objects.get(item_id=serializer_data.get('id'),
				user_id=winner)
			return_dict['winner'] = bid.user.name
			return_dict['bid_amount'] = bid.bid_amount
			return_dict['id'] = serializer_data.get('id')
		except:
			pass
	else:
		bids = Bids.objects.filter(item_id=serializer_data.get('id'))
		if bids.exists():
			bid_amount = [b.bid_amount for b in bids]
		return_dict['highest_bid_amount'] = max(bid_amount)
	return return_dict


def send_winner_details(sender_list, winner_name, bid_amount):
	subject = MAIL_SUBJECT
	mail_content = MAIL_CONTENT.format(winner_name, bid_amount)
	send_email_sendgrid(subject, sender_list, mail_content)
	return


def bid_display(queryset):
	return_arr = []
	for item in queryset:
		temp = {}
		temp['item'] = item.item_id
		temp['bid_amount'] = item.bid_amount
		return_arr.append(temp)
	return return_arr