from enassign.celery import app
from .models import AuctionItem, Bids
from .utils import send_winner_details
import traceback


@app.task
def find_winner(auction_item_id):
    """
    This celery task finds the 'winner' of each auction item once the
    end time has elapsed.
    """
    try:
        auction = AuctionItem.objects.get(pk=auction_item_id)
        bids = Bids.objects.filter(item=auction)
        max_bid_amount, winner, sender_list, winner_pk = 0, None, [], None
        if bids.exists():
            for bid in bids:
                if bid.bid_amount > max_bid_amount:
                    max_bid_amount = bid.bid_amount
                    winner = bid.user.name
                    winner_pk = bid.user.pk
                if bid.user.email not in sender_list:
                    sender_list.append(bid.user.email)
        send_winner_details(sender_list, winner, max_bid_amount)
        auction.winner_id = winner_pk
        auction.save()
        print 'Done!\n\n'
    except Exception as e:
        traceback.print_exc()