# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from datetime import datetime

from enassign.custom_fields import (CaseInsensitiveCharField,
        CaseInsensitiveEmailField)

from enassign.custom_models import BaseModel
from auction_user.models import User


# from User.models import User
# from .constants import *

''' Class to Describe Activity; An activity can have types which are 
    specified as activity choice; An activity choice has M2M relationship
    with User
'''

def item_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return '{0}/{1}'.format(instance.id, filename)

class AuctionItem(BaseModel):
    name = CaseInsensitiveCharField('Name',
            max_length=255, null=False, blank=False, db_index=True)
    description = CaseInsensitiveCharField('Name', max_length=255)
    starting_amount = models.DecimalField(decimal_places=2, max_digits=10,
            default=0.00)
    start_time = models.DateTimeField(default=datetime.now(), db_index=True)
    end_time = models.DateTimeField(default=datetime.now(), db_index=True)
    winner = models.ForeignKey(
            User, null=True, blank=False, on_delete=models.CASCADE,
            related_name='auction_winner')
    image = models.FileField(upload_to=item_directory_path)

    def __str__(self):
        return "Id -- {} -- name  -- {} -- starting_amount {} -- winner -- {}".\
            format(
            self.pk, self.name, self.starting_amount,
            self.winner.name if self.winner else '')


class Bids(BaseModel):
    user = models.ForeignKey(User, related_name='bid_user')
    item = models.ForeignKey('auction.AuctionItem', on_delete=models.CASCADE,
            related_name='bid_item')
    bid_amount = models.DecimalField(decimal_places=2, max_digits=10,
            default=0.00)

    def __str__(self):
        return "Id -- {} -- user  -- {} -- bid amount {}".format(
            self.pk, self.user.name, self.bid_amount)