from django.core.mail import send_mail

def send_email_sendgrid(subject, sender_list, content):
    send_mail(subject, content, 'test@example.com', sender_list, fail_silently=False)