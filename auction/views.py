# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
import json
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .serializers import (AuctionSerializer, BidSerializer)
from .constants import *
from .models import(AuctionItem, Bids)
from .utils import (modify_auction_response, item_bid_mapping, bid_display)
from auction_user.models import User

# from .tasks import (post_creation_notification)


class AuctionView(viewsets.ModelViewSet):
    """
    View to add ordered items for invoice line items
    """
    queryset = AuctionItem.objects.all()
    serializer_class = AuctionSerializer
    parser_classes = (MultiPartParser, FormParser)

    def retrieve(self, request, *args, **kwargs):
        auction_item = self.get_object()
        if auction_item.status == INACTIVE:
            return Response(status=HTTP_403,
                        content_type='application/json')
        serializer = self.serializer_class(auction_item)
       	modified_response = item_bid_mapping(serializer)
        return Response(status=HTTP_200,
                        data = {'auction_item': modified_response},
                        content_type='application/json')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(status=ACTIVE)
        if not queryset.exists():
            return Response(status=HTTP_403,
                        content_type='application/json')
        serializer = self.serializer_class(queryset, many=True)
        response_data = modify_auction_response(serializer)
        return Response(status=HTTP_200,
                        data = {'auction_item': response_data},
                        content_type='application/json')

    def create(self, request, *args, **kwargs):
        data = json.loads(json.dumps(request.POST.dict()))
        mandatory_params = ['starting_amount',
            'start_time', 'end_time']
        context_data = {}
        if any(map(lambda m: False if m in data else True ,
                    mandatory_params)):
            params_missing = set(mandatory_params) - set(
                                data)
            return Response(status = HTTP_400,
                    data = {MISSING_IN_REQUEST: ','.join(params_missing)},
                    content_type='application/json')
        if 'image' in request.FILES:
            print '60\n\n\n'
            context_data['image'] = request.FILES['image']
        serializer_data = {k:data[k] for k in mandatory_params}
        optional_params = ['name', 'description']
        serializer_data.update(
            {k:data[k] for k in optional_params if k in data})
        serializer = self.serializer_class(data=serializer_data, 
            context=context_data)
        if serializer.is_valid():
            auction = serializer.save()
            return Response(status = HTTP_201,
                data={AUCTION_ITEM_CREATION_SUCCESS: serializer.data},
                content_type='application/json')
        else:
            return Response(status=HTTP_400,
                data={AUCTION_ITEM_CREATION_FAILURE:serializer.errors},
                content_type='application/json')


class BidsView(APIView):

    @method_decorator(login_required(login_url='/login/'))
    def get(self, request, *args, **kwargs):
        try:
            user = request.GET['user']
        except KeyError:
            return Response(status=HTTP_400,
                data = {MISSING_IN_REQUEST: ','.join(['user'])})
        queryset = Bids.objects.filter(user_id=user)
        if not queryset.exists():
            return Response(status=HTTP_400,
                data = NO_RECORDS_PRESENT)
        return_arr = bid_display(queryset)
        return Response(status=HTTP_200,
                data = {'bid': return_arr},
                content_type='application/json')

    @method_decorator(login_required(login_url='/login/'))
    def post(self, request, *args, **kwargs):
        data = json.loads(json.dumps(request.data.dict()))
        mandatory_params = ['user', 'item', 'bid_amount']
        if any(map(lambda m: False if m in data else True ,
                    mandatory_params)):
            params_missing = set(mandatory_params) - set(
                                data)
            return Response(status = HTTP_400,
                    data = {MISSING_IN_REQUEST: ','.join(params_missing)},
                    content_type='application/json')
        serializer_data = {k:data[k] for k in mandatory_params}
        serializer = BidSerializer(data=serializer_data,
            context={'user':serializer_data.pop('user'), 
            'item': serializer_data.pop('item')})
        if serializer.is_valid():
            bid = serializer.save()
            return Response(status = HTTP_201,
                data={'bid': BID_CREATION_SUCCESS},
                content_type='application/json')
        else:
            return Response(status=HTTP_400,
                data={BID_CREATION_FAILURE:serializer.errors},
                content_type='application/json')